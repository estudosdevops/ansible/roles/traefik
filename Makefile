SHELL:=/bin/bash

deploy:
	@docker-compose up -d

destroy:
	@docker-compose down
